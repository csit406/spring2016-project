
/**
 * Module dependencies.
 */

var express = require('express')
  , flash = require('connect-flash')
  , credits = require('./routes/credits.js')
  , developers = require('./routes/developers.js')
  , routes = require('./routes')
  , roles = require('./routes/roles')
  , log = require("./routes/log")
  , region = require("./routes/region")
  , things = require("./routes/things")
  , about = require("./routes/about")
  , user = require("./routes/users")
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose'); //bringing in database 

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());

//sessions
app.use(express.cookieParser());
app.use(express.session({secret: '1234567890QWERTY'}));

//Flash messages
app.use(flash());
app.use(function(req,res,next){
  res.locals.flash = {
    error:req.flash('error'),
    notice:req.flash('notice'),
    message:req.flash('message') //how do we change the color of this type of flash message?
  };
  next();
});

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*
 * Setting up and running MongoDB in Cloud9
 * 1. mkdir data
 * 2. echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
 * 3. chmod a+x mongod
 * START
 * ./mongod
 * STOP
 * CNTRL + C
 **/
// Mongoose API http://mongoosejs.com/docs/api.html
mongoose.connect(process.env.IP);
mongoose.connection.on('connected',function(){
  console.log("Connected to MongoDB @" + process.env.IP);
});
mongoose.connection.on('disconnected',function(){
  console.log('Disconnected from MongoDB');
});
process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});


//Static Routes
app.get('/credits', credits.contacts);
app.get('/developers', developers.index);
app.get('/about', about.about);


//Node Routes
app.get('/', routes.index);
//app.all('/add', routes.add);
//app.all('/edit/:id', routes.editRegion);
//app.get('/delete/:id', routes.delete);

//Region Routes
app.get('/regions', region.index);
app.all('/regions/add', region.add);
app.all('/regions/edit/:id', region.edit);
app.get('/regions/delete/:id', region.delete);


//Things Routes
app.get('/things', things.index);
app.all('/things/add', things.add);
app.all('/things/edit/:id', things.edit);
app.get('/things/delete/:id', things.delete);

//Log Routes
app.get('/logs', log.index);
//app.all('/logs/add', log.add);
//app.all('/logs/edit/:id', log.edit);
//app.get('/logs/delete', log.delete);


/* User Routes */
app.get('/users',user.index);
app.get('/users/login',user.login);
app.all('/users/add',user.add);
app.get('/users/delete/:id',user.delete);
app.all('/users/edit/:id',user.edit);


/* Roles Routes */
/*
app.get('/roles',roles.index);
app.all('/roles/admin',roles.admin);
app.all('/roles/generaluser',roles.generaluser);
*/

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
