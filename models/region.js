var mongoose = require("mongoose")
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;
    
    
var regionSchema = new Schema({
    description: {
      type:String,
      required:true
    }
});

var Region = mongoose.model('regions',regionSchema)
exports.Region = Region;