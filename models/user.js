var mongoose = require("mongoose")
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId
    
var userSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    parent:{
        type:Boolean,
        required:false
    },
    userName:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    }
    
    
});

var User = mongoose.model('user',userSchema)
exports.User = User;