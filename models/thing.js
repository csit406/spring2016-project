var mongoose = require("mongoose")
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;
    
    
var thingSchema = new Schema({
    name: {
      type:String,
      required:true
    },
    discription: {
      type:String,
      required:true
    }
});

var Thing = mongoose.model('things',thingSchema)
exports.Thing = Thing;

//var Thing = undefined;
//exports.Thing = Thing;