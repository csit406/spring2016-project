var mongoose = require("mongoose")
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;

var logSchema = new Schema({
    user: {
        type:String,
        required:true
    },
    timestamp: {
        type:Date,
        default: Date.now
    },
    device: {
        type:String,
        required:true
    },
    message: {
        type: String,
        required:false
    }
});

var Log = mongoose.model('logger',logSchema)
exports.Log = Log;

