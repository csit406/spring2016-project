/*
*Users
*Designed by Jacob Oertle
*Date Created: 2/25/16
*Date Last Modified: 3/29/16
*/



/* Creates a list of roles */
exports.index = function(req, res)
{
    res.render('roles/index', {
        title: 'Roles List',
        roles:[
                {text:'Admin',href:'roles/admin'},
                {text:'General User',href:'roles/generaluser'}
            ]
    });
};

/*Admin Page*/
exports.admin = function(req, res)
{
    res.render('roles/admin', {title: 'Admin Page'});
};

/*General User Page*/
exports.generaluser = function(req, res)
{
    res.render('roles/generaluser', {title: 'General User Page'});
}