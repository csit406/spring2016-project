var Log = require("../models/log").Log;
 
exports.logger = function(req, res){
  res.render('log', { title: 'Log Page' });  
};

exports.index = function(req, res){
  
  var logs = undefined;
  
  Log.find({}, function(err, docs){
    if(err){
      console.log("Something Happened!!!");
      logs = [];
    }else{
      logs = docs;
    }
    res.render('logs/index', {
      title: 'Log list page',
      logs:logs
    });
  });
};

exports.add = function(username, thing, message) {
    var user = username || "undefined";
    var newLogs = new Log({
      user: user, device: thing, message: message
    }).save();
};