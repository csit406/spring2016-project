/*
*Things
*Designed by Jacob Oertle
*Date Created: 4/6/16
*Date Last Modified: 4/6/16
*/

var Thing = require("../models/thing").Thing;
var log = require("./log");
  

/* Creates a list of things */

exports.index = function(req, res){
  var things = undefined;
  
  Thing.find({},function(err,docs){
    if(err){
      console.log("My code stinks");
      things = [];
    }else{
      things = docs;
    }
    
    res.render('things/index',{
      title:'Things List',
      things:things
    });
    
  });
};

exports.add = function(req, res){
  if(req.method === "POST"){
    var newThings = new Thing({
      name:req.body.name,
      discription:req.body.discription
    }).save( function(err){console.log(err)});
    req.flash('notice','Item added sucessfully');
    log.add(req.session.username, "things", "item added");
    res.redirect('/things/');
  }else{
    res.render('things/add',{
      title:'Add Thing'
    });
  }
 
 
  
};


exports.edit = function(req,res){
  
  Thing.findById(req.params.id,function(err,doc){
    //we have an errror
    if(err){
      console.log('We have an error');
      log.add(req.session.username, "things", "Error Editing");
      res.redirect('/things/');
    }else{
    
      if(req.method === "POST"){
        console.log("klfjds");
        doc.name = req.body.name;
        doc.discription = req.body.discription;
        doc.save(err,function(){
          res.render('things/edit',{
            title:'Edit Thing',
            thing:doc
          });
          res.end();
        });
         req.flash('notice','Item updated sucessfully');
         log.add(req.session.username, "things", "item edited");
        res.redirect('/things/');
      }else{
        res.render('things/edit',{
          title:'Edit Thing',
          thing:doc
        });
      }
      
      
    }
  });
};

exports.delete = function(req, res){
  Thing.findById(req.params.id,function(err,doc){
    if(err){
      res.redirect('/things/');
      log.add(req.session.username, "things", "error deleting");
    }
    if(doc){
      doc.remove(function(err){
        req.flash('notice','Item deleted sucessfully');
        log.add(req.session.username, "things", "item deleted");
        res.redirect('/things/');
      });
    }
  });
};
