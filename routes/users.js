/*
*Users
*Designed by Spencer Gowin
*Date Created: 2/25/16
*Date Last Modified: 2/26/16
*/

/* Creates a list of users */
var User = require("../models/user").User;
//var Log = require("log");


exports.index = function(req, res)
{
    var users = undefined;
    
    User.find({},function(err, docs){
        
        if(err)
        {
            console.log("Spencer should give up his license to code");
            req.flash('error','Error loading the users');
            users = [];
        }else
        {
            users = docs;
        }
    res.render('user/index', {
        title: 'User List',
        users:users
    });
  });
};

/* Register a new user */
exports.add = function(req, res)
{
    if(req.method == "POST"){
        var newUser = new User({
            name:req.body.name,
            userName: req.body.userName,
            password: req.body.password}).save(function(err){});
                req.flash('notice',''+req.body.userName+' succesfully added');
                res.redirect('/user/');
        }else{
            res.render('/user/add',{
                title:'Create a new user'
            });
    }
};

/* Edit a User */
exports.edit = function(req, res)
{
   User.findById(req.params.id,function(err,doc){
      if(err){
          res.redirect('/user/');
          res.flash('error','Failed to edit user');
      } 
      else{
          if(req.method == "POST")
          {
              doc.userName = req.body.userName;
              doc.save(err, function(){
                  res.render('user/edit',{
                      title:'Edit a user',
                      region:doc
                  });
                  res.end();
              });
              req.flash('notice','Edited user');
              res.redirect('user/edit',{
                  title:'Edit a user',
                  region:doc
              });
          }
      }
    
   });
};

/* Login a user */
exports.login = function(req, res)
{
    
    /*
    req.session.user = "" ;
    req.session.login = false;
    
    var userInput = req.body.userInput;
    var passInput = req.body.passInput;
    //checks to see if it was empty
    if((userInput != null)&&(passInput!=null))
    {
        req.session.login = true;
    }else{
        req.flash('error', 'Login failed');
    }
    
    */
    
    res.render('user/login', {title: 'User Page'});
};


/* Delete a user */
exports.delete = function(req, res)
{
    User.findById(req.params.id, function(err,doc){
        if(err){
            req.flash('error', 'Could not delete the user');
            res.redirect('/user/');
        }
        if(doc){
            doc.remove(function(err){
                req.flash('notice','User has been permanately deleted');
                res.redirect('/user/');
            });
        }
    });
};