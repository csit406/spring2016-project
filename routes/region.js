var Region = require("../models/region").Region;
var Log = require("./log");




exports.index = function(req, res)
{
    var regions = undefined;
  
    Region.find({},function(err,docs){
        
    if(err){
      Log.add(req.session.username,"Regions", err.toString());
      req.flash('error','Error loading the content - ' + err.toString());
      regions = [];
      
    }else{
      regions = docs;
    }
    
    res.render('regions/index',{
      title:'Region Index Page',
      regions:regions
    });
    
  });
};


exports.add = function(req, res)
{
   if(req.method === "POST"){
    var newRegions = new Region({
      description:req.body.description
    }).save();
    Log.add(req.session.username,"Regions", "Successly added " + req.body.description);
    req.flash('notice','Region ' + req.body.description + ' added sucessfully');
    res.redirect('/regions/');
  }else{
    res.render('regions/add',{
      title:'Region Add Page'
    });
  }
};


exports.edit = function(req, res)
{
     Region.findById(req.params.id,function(err,doc){

    if(err){
      Log.add(req.session.username,"Regions", err.toString());
      req.flash('error','Error editing the item - ' +  req.body.description + ' - '+ err.toString());
      res.redirect('/regions/');
    }else{
    
      if(req.method === "POST"){
        doc.description = req.body.description;
        doc.save(err,function(){
          res.render('regions/edit',{
            title:'Edit Region',
            region:doc
          });
          res.end();
        });
        Log.add(req.session.username,"Regions", "Successly edited " + req.body.description);
        req.flash('notice','Region ' +  req.body.description + ' edited sucessfully');
        res.redirect('/regions/');
      }else{
        res.render('regions/edit',{
          title:'Edit Region',
          region:doc
        });
      }
    }
  });
};


exports.delete = function(req, res)
{
    Region.findById(req.params.id,function(err,doc){
    var deleted = doc.description;
    if(err){
      req.flash('error','Error deleting ' +  req.body.description + ' - ' + err.toString());
      Log.add(req.session.username,"Regions", err.toString());
      res.redirect('/regions/');
    }
    if(doc){
      doc.remove(function(err){
        req.flash('notice','Region deleted sucessfully');
        Log.add(req.session.username,"Regions", "Successly deleted " + deleted);
        res.redirect('/regions/');
      });
    }
  });
};
