//just a bit of old code to help get started on AJAX
//Top level hierarchy page

var Ajax = function(){
    var defaults = {
        method:'GET',
        success:function(data){console.log(data);},
        failure:function(statusText){console.log(statusText);},
        data:''
    };
    
    
    this.makeRequest = function(url,options){
        var xmlhttp = new XMLHttpRequest();
        
        for(var n in defaults){
            options[n] = (options[n] !== undefined) ? options[n] : defaults[n];
        }
        
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState === 4){
                if(xmlhttp.status === 200){
                    options.success(xmlhttp.responseText);
                }else{
                    options.failure(xmlhttp.statusText);
                }
            }
        };
        
        xmlhttp.open(options.method,url,true);
        xmlhttp.setRequestHeader('x-requested-with','XMLHttpRequest');
        xmlhttp.send(options.data);
        
    };
};

var HtmlAjax = function(contentArea,beforeRequest,afterRequest){
    Ajax.call(this);
    
    var o = this;
    var before = (typeof beforeRequest === 'function') ? beforeRequest : function(contentArea){console.log('before');};
    var after = (typeof afterRequest === 'function') ? afterRequest : function(contentArea){console.log('after')};
    
    window.addEventListener('popstate',function(e){
        before(contentArea);
        o.makeRequest(document.location,{
            success:function(data){
                contentArea.innerHTML = data;
                after(contentArea);
            },
            failure:function(statusText){
                window.location.href = document.location;
            }
        });
    },false);
    
    o.get = function(url){
        before(contentArea);
        o.makeRequest(url,{
           success:function(data){
               window.history.pushState({},"",url);
               contentArea.innerHTML = data;
               after(contentArea);
           },
           failure:function(statusText){
               window.location.href = url;
           }
        });
    };
};
HtmlAjax.prototype = Object.create(Ajax.prototype);
HtmlAjax.contructor = Ajax.contructor;
