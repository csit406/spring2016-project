# Class Project
## About
Project Description

## Contributors

## Agile Artifacts
Find at [trello.com](https://trello.com/csit406spring2016)

## Adding your code to the project
1. Update your local repository from bitbucket
    ```shell
    git pull --rebase
    ```
2. Verify that your code still works and fix any issues
3. Stage any new files to be committed. Hint: * is a wildcard that can add more than one file.
    ```shell
    git add app.js
    git add routes/user.js
    git add views/user/*
    ```
4. Commit all your changes to the repository
    ```shell
    git commit -m "Added the User Module"
    ```
5. Push your changes to bitbucket
    ```shell
    git push origin master
    ```