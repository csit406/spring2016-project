var nodes = [
    {//node object like house
        id:{/*unique primary key*/},
        description:{},
        permissions:{/* enumeration view, edit, control */},
        
        regions:[
            {//region object like room
                id:{/*unique primary key*/},
                description:{},
                permissions:{/* enumeration view, edit, control */},
                
                
                things:[
                    {//thing object like 
                        id:{/*unique primary key*/},
                        description:{/* user friendly */},
                        permissions:{/* enumeration view, edit, control */},
                        active:{/* on/off */},
                        state:{},
                        notify:{},
                        address:{}
                        
                    }
                ]
            }   
        ]
    }  
];